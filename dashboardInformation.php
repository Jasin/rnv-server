<?php

$avgDelay = json_decode(getAVGPerDelay());
$sumToday = json_decode(getSumValue());

	$encode = array
	(
	    'avgToday' => $avgDelay, 
	    'sumToday' => $sumToday
	);
	echo json_encode($encode);

function getSumValue()
{
	require ("db.php");

    $sql  = 'select CAST(created AS DATE) as "created", SUM(delay) as "average" from tours group by CAST(created AS DATE) order by created desc;';

    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        die ("Fehler beim lesen der Datenbank");
    }

    $temp = array();

    while($row = $res->fetch_assoc())
    {
        array_push($temp, $row);
    }

    return (json_encode($temp[0]));
}

function getAVGPerDelay()
{
	require ("db.php");

    $sql  = 'SELECT AVG(delay) FROM `tours`;';

    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        die ("Fehler beim lesen der Datenbank");
    }

    $temp = array();

    while($row = $res->fetch_assoc())
    {
        array_push($temp, $row);
    }

    return (json_encode($temp[0]));
}