<?php
header('Access-Control-Allow-Origin: *');

$generalDelay = getSumDelay();
$information = getLastTen();

$information =
    [
        "generalDelays" => $generalDelay,
        "lastDelays" => $information
    ];

echo (json_encode($information));

function getSumDelay()
{
    require ("db.php");

    $sql = "SELECT SUM(delay) FROM tours;";
    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        logIt(ERRORLOG, "There was an Error while writing in db " . $conn->error, __DIR__ . __FILE__);
        die ("Fehler beim lesen der Daten in die Datenbank");
    }
    $generalDelay = mysqli_fetch_array($res)[0];
    return $generalDelay;
}

function getLastTen()
{
    require ("db.php");
    $sql  = 'select * from tours order by id desc limit 10';
    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        logIt(ERRORLOG, "There was an Error while writing in db " . $conn->error, __DIR__ . __FILE__);
        die ("Fehler beim Eintragen der Daten in die Datenbank");
    }
    else
        {
        if (mysqli_num_rows($res) > 0)
        {
            $tenStops = array();
            while ($row = mysqli_fetch_assoc($res))
            {
                array_push($tenStops, $row);
            }
        }
        mysqli_close($conn);
    }

    return $tenStops;
}