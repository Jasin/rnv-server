<?php
header('Access-Control-Allow-Origin: *');

require ("db.php");

$sql  = "SELECT CAST(created AS DATE) as 'created', SUM(delay) as 'delays' from tours group by CAST(created AS DATE)";

$res = mysqli_query($conn, $sql);
if ($conn->errno)
{
    var_dump($conn);
    die ("Fehler beim lesen der Datenbank");
}

$temp = array();

while($row = $res->fetch_assoc())
{
    array_push($temp, $row);
}

die(json_encode($temp));