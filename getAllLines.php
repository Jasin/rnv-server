<?php
header('Access-Control-Allow-Origin: *');

require ("db.php");

$sql  = "SELECT lineLabel as 'lineLabel' from tours group by lineLabel";

$res = mysqli_query($conn, $sql);
if ($conn->errno)
{
    var_dump($conn);
    die ("Fehler beim lesen der Datenbank");
}

$temp = array();

while($row = $res->fetch_assoc())
{
    array_push($temp, $row);
}

die(json_encode($temp));