All information (german only) on rnv-monitor.de
To clone this project, make a folder named "private" in the same direction like this project and rename the cloned folder "public".
Go into the private folder and make a file named clientSecret with the following content:
{
"servername":"YOUR_SERVER_NAME e.g. localhost",
"dbName":"NAME_OF_YOUR_DB",
"dbUsername":"NAME_OF_DB_USER",
"dbPassword":"PASSWORD_OF_YOUR_DB_USER",
"RNV_API_KEY":"API_KEY"
}
You get an API Key on
https://opendata.rnv-online.de/startinfo-api
Have fun!
PS.: Use the DB Dump, I had no time to auto generate one :)