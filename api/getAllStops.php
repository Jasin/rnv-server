<?php
//1245 Stations
function getAllStops()
{
    try
    {
        $config = json_decode(file_get_contents('config'));
        $config = get_object_vars($config);
    }
    catch(Exception $e)
    {
        logIt(ERRORLOG, "Could not read local config file - error " . $e, __DIR__ . __FILE__);
    }

    $secret = null;

    try
    {
        $secret = json_decode(file_get_contents('../private/clientSecret'));
        $secret = get_object_vars($secret);
    }
    catch(Exception $e)
    {
        logIt(ERRORLOG, "Could not read local API_KEY - error " . $e, __DIR__ . __FILE__);
    }

    if($secret == null)
    {
        logIt(ERRORLOG, "Couldn't read clientSecret", __FILE__ . __LINE__);
        return 0;
    }
    else
    {
        logIt(DEBUGLOG, "Start getting Information from RNV about Stops", __FILE__ . __LINE__);
        $curl = curl_init();
        curl_setopt_array($curl, array
        (
            CURLOPT_PORT => "8080",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_URL => $config['RnvApiBaseURL'] . "/regions/rnv/modules/stations/packages/1",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "RNV_API_TOKEN: ".$secret['RNV_API_KEY'],
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err)
        {
            logIt(ERRORLOG, "Curl Error - " . $err, __FILE__ . __LINE__);
            return 0;
        } else
        {
            logIt(DEBUGLOG, "Successfully got Stops from RNV, read them", __FILE__ . __LINE__);
            //check if returned stop is already known, if not, write it in db
            $response = json_decode($response);
            for($i = 0; $i < count($response->stations); $i++)
            {
                if(!isStopAlreadyKnown($response->stations[$i]->hafasID))
                {
                    $temp = $response->stations[$i];
                    if(!writeStopInDB($temp->longName, $temp->longitude, $temp->latitude, $temp->hafasID))
                    {
                        logIt(ERRORLOG, "Failed writing in DB number " . $i . " try it again 3 times", __FILE__ . __LINE__);
                        for($i = 0; $i < 2; $i++)
                        {
                            if(!writeStopInDB($temp->longName, $temp->longitude, $temp->latitude, $temp->hafasID))
                            {
                                logIt(ERRORLOG, "Failed again - try Number: " . ($i + 1), __FILE__ . __LINE__);
                            }
                            else
                            {
                                logIt(DEBUGLOG, "Successfully wrote in DB at attempt: " . ($i + 1), __FILE__ . __LINE__);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    logIt(DEBUGLOG, "Station already known: " . $response->stations[$i]->hafasID, __FILE__. __LINE__);
                }
            }
            return json_encode($response, JSON_PRETTY_PRINT);
        }
    }
}

function writeStopInDB($name, $long, $lat, $hafasID)
{
    require ('db.php');

    $sql = "INSERT INTO `stops` (`name`, `longitude`, `latitude`, `hafasID`) VALUES ('$name', '$long', '$lat', $hafasID);";
    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        logIt(ERRORLOG, "There was a DB Error " . $conn->error, __FILE__ . __LINE__);
        return false;
    }
    else
    return true;
}

function isStopAlreadyKnown($hafasID)
{
    require ('db.php');

    if(isset($hafasID))
    {
        $sql = "SELECT * FROM `stops` WHERE `hafasID`='$hafasID'";
        $res = mysqli_query($conn, $sql);
        if ($conn->errno)
        {
            logIt(ERRORLOG, "Coudn't read from DB, Error was: " . $conn->error, __DIR__ . __FILE__);
        }
        $check = mysqli_num_rows($res);
        if ($check == 0)
        {
            return false;
        }else
            {
                return true;
            }
    }
    else
        {
            logIt(WARNINGLOG, "isStopAlreadyKnown get wrong parameter", __FILE__ . __LINE__);
            return true;
        }
}