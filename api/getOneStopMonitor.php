<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);


function monitorStation($hafasID)
{
    $hafasID = strip_tags($hafasID);
    $secret = null;
    try
    {
        $secret = json_decode(file_get_contents('../private/clientSecret'));
        $secret = get_object_vars($secret);
    } catch (Exception $e)
    {
        logIt(ERRORLOG, "Could not read local API_KEY - error " . $e, __DIR__ . __FILE__);
    }

    if($secret == null)
    {
        logIt(ERRORLOG, "clientSecret was NULL ", __DIR__ . __FILE__);
        return false;
    }
    else
    {
        $curl = curl_init();
        curl_setopt_array($curl, array
        (
            CURLOPT_PORT => "8080",
            CURLOPT_URL => "http://rnv.the-agent-factory.de:8080/easygo2/api/regions/rnv/modules/stationmonitor/element?hafasID=$hafasID&time=null",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 45,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "RNV_API_TOKEN: " . $secret['RNV_API_KEY'],
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            logIt(ERRORLOG, "Curl error: " . $err, __FILE__ . __DIR__);
            return false;
        }
        else
        {
            try
            {
                $response = json_decode($response);
            }
            catch(Exception $e)
            {
                logIt(ERRORLOG, "Couldn't json_Decode response of RNV", __FILE__ . __LINE__);
                return false;
            }

            $stops = $response->listOfDepartures; //Should be an Array of 10 lines of this stop
            if (sizeof($stops) != 10)
            {
                logIt(WARNINGLOG, "RNV didn't return 10 stops or couldn't read them", __FILE__ . __LINE__);
            }
            else
            {
                for ($i = 0; $i < sizeof($stops); $i++)
                {
                    if($stops[$i]->kindOfTour != "454AUS")
                    {
                        logIt(WARNINGLOG, "Only Soll-Abfahrtzeiten for: " . $hafasID . " " . $stops[$i]->lineLabel, __FILE__.__LINE__);
                    }
                    else
                    {

                        //Check if this Stop is the first stop in this tour
                        if ($stops[$i]->positionInTour == 1)
                        {
                            //Separate Delay and Sollabfahrtzeit
                            if (strpos($stops[$i]->time, "+") !== false)
                            {
                                $temp = explode('+', $stops[$i]->time); //temp[0] should be "sollabfahrzeit", temp[1] should be delay
                            }
                            else
                            {
                                $temp[0] = $stops[$i]->time;
                                $temp[1] = 0; //TODO: ERRORHANDLING
                            }


                            try
                            {
                                $specTourID = $stops[$i]->tourId . "_" . $temp[0] . "_" . $stops[$i]->lineLabel;
                            }
                            catch(Exception $exception)
                            {
                                //TODO: ERRORHANDLING
                                $specTourID = "ERROR";
                            }


                            //TODO: Crashes here (Recoverable fatal error: Object of class stdClass could not be converted to string in /var/www/html/public/server/api/getOneStopMonitor.php on line 100)
                            if (isTourAlreadyKnown($specTourID))
                            {
                                //TODO: Check for updates
                                logIt(DEBUGLOG, "Tour already known try to update", __FILE__ . __LINE__);
                            }
                            else
                            {
                                logIt(DEBUGLOG, "Current Stop is first Stop in a Tour and not known. Write in DB", __FILE__ . __LINE__);
                                $averageDelay = requestMonitorForThisTour($stops[$i]->lineLabel, $temp[0], $stops[$i]->tourId, $hafasID, $stops[$i]->kindOfTour);
                                //TODO: $averageDelay will be false on function error, this will cause an error in func writeDB cause sql expects an integer
                                if($averageDelay == 0)
                                {
                                    logIt(DEBUGLOG, "Huray, no Delay", __FILE__.__LINE__);
                                }
                                else if($averageDelay < 0)
                                {
                                    logIt(ERRORLOG, "averageDelay was smaller then 0: " . $averageDelay, __FILE__.__LINE__);
                                }
                                else
                                {
                                    if(!writeDelayInDB($specTourID, $averageDelay, $stops[$i]->lineLabel, $stops[$i]->direction, $stops[$i]->transportation, $stops[$i]->tourId, $hafasID))
                                    {
                                        logIt(ERRORLOG, "Failed writing in DB number " . $i . " try it again 3 times", __FILE__ . __LINE__);
                                        for($i = 0; $i < 2; $i++)
                                        {
                                            if(!writeDelayInDB($specTourID, $averageDelay, $stops[$i]->lineLabel, $stops[$i]->direction, $stops[$i]->transportation, $stops[$i]->tourId, $hafasID))
                                            {
                                                logIt(DEBUGLOG, "Failed again - try Number: " . ($i + 1), __FILE__ . __LINE__);
                                            }
                                            else
                                            {
                                                logIt(DEBUGLOG, "Successfully wrote in DB at attempt: " . ($i + 1), __FILE__ . __LINE__);
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        logIt(SuccessLOG, "Successfully wrote a delay of " . $averageDelay . " in DB", __FILE__ . __LINE__);
                                    }
                                }
                            }
                        }
                        else
                        {
                            logIt(DEBUGLOG, "Not first stop in this tour, do nothing " . $i, __FILE__ . __LINE__);
                        }
                    }
                }
            }
        }
    }
}

    function isTourAlreadyKnown($specTourID)
    {
        require('db.php');

        $sql = "SELECT * from `tours` WHERE `specID` = '$specTourID';";
        $res = mysqli_query($conn, $sql);
        if ($conn->errno)
        {
            logIt(ERRORLOG, "There was an Error while reading from db " . $conn->error, __DIR__ . __FILE__);
            return true;
        }
        else
            {
                if (mysqli_num_rows($res) > 0)
                {
                    mysqli_close($conn);
                    return true;
                } else
                    {
                    mysqli_close($conn);
                    return false;
                }
            }
    }

    function writeDelayInDB($specID, $delay, $lineLabel, $direction, $transportation, $tourID, $hafasID)
    {
        require('db.php');
        logIt(DEBUGLOG, "Write in Db was called with: specID " . $specID . " delay: " . $delay . " lineLabel: " . $lineLabel . " direction: " . $direction . " transportation: " . $transportation . " tourID: " . $tourID . " hafasID: " . $hafasID, __FILE__.__LINE__);
        $sql = "INSERT INTO `tours` (`specID`, `delay`, `lineLabel`, `direction`, `transportation`, `tourID`) VALUES ('$specID', '$delay', '$lineLabel', '$direction', '$transportation', '$tourID');";
        $res = mysqli_query($conn, $sql);
        if ($conn->errno) {
            logIt(ERRORLOG, "There was an Error while writing in db " . $conn->error, __DIR__ . __FILE__);
            return false;
        } else
            mysqli_close($conn);
        return true;
    }

    function requestMonitorForThisTour($lineLabel, $time, $tourID, $hafasID, $kindOfTour)
    {
        date_default_timezone_set('Europe/Berlin');
        /*time parameter should be Sollabfahrtzeit - format: dd.mm.yyyy HH:mm
        expected return value: average delay of this tour as a int*/

        $date = date("d.m.Y"); //Returns something like 01.01.2019

        //Get RNV-API token
		$secret = null;
		try
		{
			$secret = json_decode(file_get_contents('../private/clientSecret'));
			$secret = get_object_vars($secret);
		} catch (Exception $e)
		{
			logIt(ERRORLOG, "Could not read local API_KEY - error " . $e, __DIR__ . __FILE__);
		}

        if ($secret == null)
        {
            return false;
        } else {
            logIt(DEBUGLOG, "Start getting information from RNV about: lineLabel " . $lineLabel . " time " . $time . " tourId " . $tourID . " hafasID " . $hafasID, __FILE__ . __LINE__);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_PORT => "8080",
                CURLOPT_URL => "http://rnv.the-agent-factory.de:8080/easygo2/api/regions/rnv/modules/lines?lineID=$lineLabel&stopIndex=1&time=$date%20$time&tourType=$kindOfTour&tourID=$tourID&hafasID=$hafasID",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 50,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_USERAGENT => "Mozilla",
                CURLOPT_COOKIE => "troute=t1",
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array
                (
                    "RNV_API_TOKEN: " . $secret['RNV_API_KEY'],
                    "cache-control: no-cache",
                    "Content-Type: application/json",
                    "content-type: application/Json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            if ($err)
            {
                logIt(ERRORLOG, "There was a curl error: " . $err, __FILE__ . __LINE__);
                curl_close($curl);
                return false;
            }
            else if(!$response)
            {
                logIt(ERRORLOG, "There was a curl error: response was false" . $err, __FILE__ . __LINE__);
                curl_close($curl);
                return false;
            }
            else
            {
				curl_close($curl);

				$times = json_decode($response);
				$times = $times->timeList;
				$delays = [];
				for ($i = 0; $i < sizeof($times); $i++)
				{
					if (!strpos($times[$i], '+'))
					{
						logIt(WARNINGLOG, "Didn't find a delay for $i", __FILE__ . __LINE__);
					} else 
					{
						//Separate time
						$temp = explode("+", $times[$i]); //temp[0] == Solltime, $temp[1] is delay
						array_push($delays, $temp[1]);
					}
				}
				if(count($delays) <= 0)
				{
					logIt(WARNINGLOG, "Didn't find any delays for this tour", __FILE__ . __LINE__);
					return false;
				}
				else
				{
					$average = array_sum($delays) / count($delays);
					logIt(DEBUGLOG, "Got an average Delay of: $average", __FILE__ . __LINE__);
					return $average;
				}
            }
        }
    }


/*function updateDelay($specID, $newDelay)
{
    require ('db.php');
    $sql = "UPDATE `delays` SET delay = '$newDelay' WHERE specID = '$specID'";
    $res = mysqli_query($conn, $sql);
    if($conn->errno)
    {
        logIt(ERRORLOG, "There was an Error while reading from db " . $conn->error, __DIR__ . __FILE__);
        die ("Fehler beim Eintragen der Daten in die Datenbank");
    }
}*/