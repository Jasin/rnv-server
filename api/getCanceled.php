<?php
function getAllCanceledLines()
{
    try
    {
        $config = json_decode(file_get_contents('config'));
        $config = get_object_vars($config);
    }
    catch(Exception $e)
    {
        logIt(ERRORLOG, "Could not read local config file - error " . $e, __DIR__ . __FILE__);
    }

    $secret = null;

    try
    {
        $secret = json_decode(file_get_contents('../private/clientSecret'));
        $secret = get_object_vars($secret);
    }
    catch(Exception $e)
    {
        logIt(ERRORLOG, "Could not read local API_KEY - error " . $e, __DIR__ . __FILE__);
    }

    if($secret == null)
    {
        logIt(ERRORLOG, "Couldn't read clientSecret", __FILE__ . __LINE__);
        return 0;
    }
    else
    {
        logIt(DEBUGLOG, "Start getting Information from RNV about Canceled", __FILE__ . __LINE__);
        $curl = curl_init();
        curl_setopt_array($curl, array
        (
            CURLOPT_PORT => "8080",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_URL => $config['RnvApiBaseURL'] . "/regions/rnv/modules/canceled/line",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "RNV_API_TOKEN: ".$secret['RNV_API_KEY'],
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

       	if ($err)
        {
            logIt(ERRORLOG, "Curl Error while getting Canceled - " . $err, __FILE__ . __LINE__);
            return 0;
        } else
        {
            logIt(DEBUGLOG, "Successfully got Canceled from RNV, read them", __FILE__ . __LINE__);

            $response = json_decode($response);
            if($response == null || $response == "")
            {
            	logIt(WARNINGLOG, "RNV answer was no JSON!", __FILE__ . __LINE__);
            	return 0;
            }
            else
            {
				var_dump($response);
				return 0;
            }
        }
    }	
}

function isCancelAreadyKnown()
{

}

function saveCancel()
{

}