<?php

header('Access-Control-Allow-Origin: *');
$lineLabel = $_GET['lineLabel'];
$lineLabel = $_GET['date'];

$lineLabel = '"' . $lineLabel . '"';

if(!isset($lineLabel))
{
    die("You have to give me a lineLabel via get! :(");
}
else
{
	$maxValueJson = json_decode(getMaxValue($lineLabel));
	$minValueJson = json_decode(getMinValue($lineLabel));
	$averageValueJson = json_decode(getAverageValue($lineLabel));

	$encode = array
	(
	    'max' => $maxValueJson, 
	    'min' => $minValueJson,
	    'average' => $averageValueJson
	);
	echo json_encode($encode);
}

function getAllFromToday($lineLabel)
{
    //select * from rnv.tours where lineLabel = '23' and DATE(created) ='2019-04-15'
}

function getAverageValue($lineLabel)
{
	require ("db.php");

    $sql  = 'select CAST(created AS DATE) as "created", AVG(delay) as "average" from tours where lineLabel = ' . $lineLabel .  ' group by CAST(created AS DATE) order by created desc;';

    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        die ("Fehler beim lesen der Datenbank");
    }

    $temp = array();

    while($row = $res->fetch_assoc())
    {
        array_push($temp, $row);
    }

    return (json_encode($temp[0]));
}

function getMinValue($lineLabel)
{
	require ("db.php");

    $sql  = 'select CAST(created AS DATE) as "created", MIN(delay) as "min" from tours where lineLabel = ' . $lineLabel .  ' group by CAST(created AS DATE) order by created desc;';

    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        die ("Fehler beim lesen der Datenbank");
    }

    $temp = array();

    while($row = $res->fetch_assoc())
    {
        array_push($temp, $row);
    }

    return (json_encode($temp[0]));
}

function getMaxValue($lineLabel)
{
	require ("db.php");

    $sql  = 'select CAST(created AS DATE) as "created", MAX(delay) as "max" from tours where lineLabel = ' . $lineLabel .  ' group by CAST(created AS DATE) order by created desc;';

    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        die ("Fehler beim lesen der Datenbank");
    }

    $temp = array();

    while($row = $res->fetch_assoc())
    {
        array_push($temp, $row);
    }

    return (json_encode($temp[0]));
}