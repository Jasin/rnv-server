<?php
header('Access-Control-Allow-Origin: *');
$lineLabel = $_GET['lineLabel'];
$amount = $_GET['amount'];

$lineLabel = '"' + $lineLabel + '"';

if(!isset($lineLabel))
{
    die("You have to give me a lineLabel via get! :(");
}
else
{
    require ("db.php");

    $sql  = 'select CAST(created AS DATE) as "created", SUM(delay) as "delays" from tours where lineLabel = ' . $lineLabel . ' group by CAST(created AS DATE) limit ' . $amount;

    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        die ("Fehler beim lesen der Datenbank");
    }

    $temp = array();

    while($row = $res->fetch_assoc())
    {
        array_push($temp, $row);
    }

    die(json_encode($temp));
}
