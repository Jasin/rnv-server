<?php

error_reporting(E_ALL);

require_once ("LogIt.php");
require_once ("api/getAllStops.php");
require_once ("api/getOneStopMonitor.php");
require_once ("api/getCanceled.php");

while(1)
{
    $allStops = null;
    $countAllStops = 0;

    //Canceled Lines Logic
    //getAllCanceledLines();
    
    //Delay Logic
    try
    {
        $allStops = getAllStops();
        $allStops = json_decode($allStops);
        $allStops = $allStops->stations;
        $countAllStops = count($allStops);
    }
    catch(Exception $exception)
    {
        logIt(ERRORLOG, "Coudln't json_decode all Stops! Function returned: ", __FILE__.__LINE__);
        var_dump($allStops);
        logIt(ERRORLOG, "Exeption was: ", __FILE__. __LINE__);
        var_dump($exception);
    }

    if($allStops == null || $countAllStops <= 0)
    {
        logIt(ERRORLOG, "LOOP got 0 or lower stops, try to get them again", __FILE__.__LINE__);
        while(1)
        {
            $allStops = getAllStops();
            $allStops = json_decode($allStops);
            $allStops = $allStops->stations;
            if(count($allStops) > 1)
            {
                break;
            }
        }
    }
    else
    {
        for($i = 0; $i <= $countAllStops; $i++)
        {
            monitorStation($allStops[$i]->hafasID);
        }
    }
}
