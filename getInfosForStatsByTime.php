<?php
header('Access-Control-Allow-Origin: *');

if(!isset($_GET['endDate']))
{
    die("Missing parameter endDate");
}
if(!isset($_GET['startDate']))
{
    die("Missing parameter startDate");
}
if(!isset($_GET['lineLabel']))
{
    die("Missing parameter lineLabel");
}

$lineLabel = '"' . $_GET['lineLabel'] . '"';
$startDate = '"' . $_GET['startDate'] . '"';
$endDate = '"' . $_GET['endDate'] . '"';

require ("db.php");

$sql  = "SELECT CAST(created AS DATE) as 'created', ROUND(SUM(delay), 2) as 'delays' from tours where created >= " . $startDate . " AND created <= " . $endDate  . " AND lineLabel = " . $lineLabel . " group by CAST(created AS DATE)";

$res = mysqli_query($conn, $sql);
if ($conn->errno)
{
    die ("Fehler beim lesen der Datenbank");
}

$temp = array();

while($row = $res->fetch_assoc())
{
    array_push($temp, $row);
}

die(json_encode($temp));
