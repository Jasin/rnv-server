<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

$config = file_get_contents("../private/clientSecret");
$database_information = json_decode($config, true);

if(empty($database_information))
{
    die('500: There was no config for db info found!');
}

$servername = $database_information['servername'];
$username = $database_information['dbUsername'];
$password = $database_information['dbPassword'];
$dbname = $database_information['dbName'];

$conn = new mysqli($servername, $username, $password);
if ($conn->connect_error)
{
    die("Couldn't build a connection to SQL-Server: ".$conn->connect_error);
}

//Check if DB exists
$sql = "SHOW DATABASES LIKE '$dbname'";
if (!mysqli_num_rows($conn->query($sql)) == 1)
{
    echo ("DB does not exists, create one<br>".$conn->error);
    $sql = "CREATE DATABASE $dbname";
    if ($conn->query($sql) === TRUE)
    {
        echo "Database was created successfully!";
    } else
        {
            die("There was a fatal Error! Upsi ".$conn->error);
        }
}
$conn->select_db($dbname);
$names = mysqli_query($conn, "SET NAMES 'utf8'");
